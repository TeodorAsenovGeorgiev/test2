package bg.codexio.people.repository;

import bg.codexio.people.entity.Calculator;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface CalculatroRepository extends JpaRepository<Calculator, Long> {

      List<Calculator> findByOperation(String operation);
      Calculator findByDateAndOperation(Date date, String operation);
      Calculator findAllById(Long id);


}
