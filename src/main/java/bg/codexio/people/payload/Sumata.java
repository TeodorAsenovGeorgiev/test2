package bg.codexio.people.payload;

public class Sumata {
    private Long suma;

    public Sumata() {
    }

    public Sumata(Long suma) {
        this.suma = suma;
    }

    public Long getSuma() {
        return suma;
    }

    public void setSuma(Long suma) {
        this.suma = suma;
    }
}
