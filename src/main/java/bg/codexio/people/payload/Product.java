package bg.codexio.people.payload;

public class Product {
    private Long product;


    public Product() {
    }


    public Product(Long product) {
        this.product = product;
    }

    public Long getProduct() {
        return product;
    }

    public void setProduct(Long product) {
        this.product = product;
    }
}