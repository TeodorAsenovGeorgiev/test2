package bg.codexio.people.controller;

import bg.codexio.people.entity.Calculator;
import bg.codexio.people.payload.CalculatorRequest;
import bg.codexio.people.payload.Product;
import bg.codexio.people.payload.Sumata;
import bg.codexio.people.service.CalculatorService;
import org.springframework.web.bind.annotation.*;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
public class CalculatorController {
    private final CalculatorService calculatorService;


    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;

    }


    @PostMapping("calculate/{num1}/{num2}/suma")
    @ResponseBody
    public Sumata sum(@PathVariable Long num1, @PathVariable Long num2){
        return calculatorService.sum(num1,num2);
    }

    @PostMapping("calculate/{num1}/{num2}/multiply")
    @ResponseBody
    public Product multiply(@PathVariable Long num1, @PathVariable Long num2){
        return calculatorService.multiply(num1,num2);
    }

    @PostMapping("update/{newNum1}/{newNum2}/{operation}/{date}")
    @ResponseBody
    public Calculator update(
            @PathVariable  Long newNum1,
            @PathVariable  Long newNum2,
            @PathVariable  String operation,
            @PathVariable  String  date
        ) throws ParseException {
        Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(date);
        return this.calculatorService.update(newNum1, newNum2, operation, date1);

    }
    @PutMapping("updateP/")
    @ResponseBody
    public Calculator updateP(@RequestBody CalculatorRequest calculatorRequest) throws ParseException {
        return this.calculatorService.updateP(calculatorRequest);
    }

        @GetMapping("read/{operation}")
        @ResponseBody
        public List <Calculator> findByOperation(@PathVariable String operation) {

            return this.calculatorService.findByOperation(operation);
        }

        @DeleteMapping("delete/{id}")
        @ResponseBody
        public List<Calculator> delete(@PathVariable Long id){
        return  this.calculatorService.delete(id);
        }


}
