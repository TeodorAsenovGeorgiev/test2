package bg.codexio.people.service;

import bg.codexio.people.entity.Calculator;
import bg.codexio.people.payload.CalculatorRequest;
import bg.codexio.people.payload.Product;
import bg.codexio.people.payload.Sumata;

import java.text.ParseException;
import java.util.Date;
import java.util.List;


public interface CalculatorService {
    Sumata sum(Long num1, Long num2);

    Product multiply(Long num1, Long num2);

    List<Calculator>findByOperation(String operation);

   Calculator update(Long num1, Long num2, String operation, Date date);

   List<Calculator> delete(Long id);

   Calculator updateP(CalculatorRequest calculatorRequest) throws ParseException;



}
