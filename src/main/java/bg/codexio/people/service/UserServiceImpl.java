package bg.codexio.people.service;

import bg.codexio.people.entity.User;
import bg.codexio.people.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public User create(Long userId, String name) {
       User user = new User();
       User user2 = new User();
       user.setId(userId);
        user.setName(name+name);
        user2.setId(userId+1L);
        user2.setName(name+name);
        this.userRepository.save(user);
        this.userRepository.save(user2);


        return user;
    }


}
