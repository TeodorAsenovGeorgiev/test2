package bg.codexio.people.service;

import bg.codexio.people.entity.Calculator;
import bg.codexio.people.payload.CalculatorRequest;
import bg.codexio.people.payload.Product;
import bg.codexio.people.payload.Sumata;
import bg.codexio.people.repository.CalculatroRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;


@Service
@Transactional
public class CalculatorServiceImpl implements CalculatorService {
    private final CalculatroRepository  calculatroRepository;

    public CalculatorServiceImpl(CalculatroRepository calculatroRepository) {
        this.calculatroRepository = calculatroRepository;
    }

    @Override
    public Sumata sum(Long num1, Long num2) {
       Sumata sumata = new Sumata();
        Calculator calculator = new Calculator();
        calculator.setNum1(num1);
        calculator.setNum2(num2);
        calculator.setOperation("sum");
       // calculator.setDate(Date.from(Instant.now()));
       // calculator.setDate(LocalDateTime.now());
        calculator.setDate(timeNow());
        long resultSum = num1+num2;
        calculator.setResult(resultSum);
        sumata.setSuma(resultSum);
        this.calculatroRepository.save(calculator);
        return sumata;
    }

    @Override
    public Product multiply(Long num1, Long num2) {
        Product product = new Product();
        product.setProduct(num1*num2);
        Calculator calculator = new Calculator();
        //Date from = Date.from(Instant.now());
        //from.setTime((from.getTime() / 1000) * 1000);
        calculator.setDate(timeNow());
       // //calculator.setDate(LocalDateTime.now());
         calculator.setNum1(num1);
         calculator.setNum2(num2);
         calculator.setOperation("multiply");
         calculator.setResult(product.getProduct());
         this.calculatroRepository.save(calculator);
        return product;
    }

    public Date timeNow(){
        Date noMilliseconds = Date.from(Instant.now());
        noMilliseconds.setTime((noMilliseconds.getTime()/1000)*1000);
        return noMilliseconds;
    }

    @Override
    public Calculator update (Long newNum1, Long newNum2, String operation, Date date) {
        //1. След като намерим калкулатора, искаме да презапишем датата,на която сме извършили арихметиката и самата арихметика

        var calculator = this.calculatroRepository.findByDateAndOperation(date,operation);
        calculator.setNum1(newNum1);
        calculator.setNum2(newNum2);
        calculator.setResult(newNum1*newNum2);
        calculator.setDate(timeNow());


        return calculator;
      }

    @Override
    public List<Calculator> delete(Long id) {

        var delete = this.calculatroRepository.findAllById(id);
        this.calculatroRepository.delete(delete);
        return this.calculatroRepository.findAll();
    }

    @Override
    public Calculator updateP(CalculatorRequest calculatorRequest) throws ParseException {
        var calculator = this.calculatroRepository.findByDateAndOperation(calculatorRequest.getDate(),calculatorRequest.getOperation());
        calculator.setNum1(calculatorRequest.getNum1());
        calculator.setNum2(calculatorRequest.getNum2());
        calculator.setResult(calculatorRequest.getNum1()*calculatorRequest.getNum2());
        calculator.setDate(timeNow());
        return calculator;
    }


    @Override
    public List<Calculator> findByOperation(String operation) {


        return this.calculatroRepository.findByOperation(operation);
    }
}



