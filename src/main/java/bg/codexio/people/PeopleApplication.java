package bg.codexio.people;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class PeopleApplication {

    public static void main(String[] args) {
     SpringApplication.run(PeopleApplication.class, args);

    }


}
